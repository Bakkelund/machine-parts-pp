# machine-parts-pp

---

**This repository contains the following three assets:**

- An data sample of machinery data tables of both material types (anonymised) and part-of relations.
- Dissimilarity data for the sample, obtained from the Excess Inventory project
- A python implementation offering planted partition generation based on the data
- The $\LaTeX$ code of the data article describing both the data and the planted partition generator

---

For an in-depth introduction, please read the data article: <https://bitbucket.org/Bakkelund/machine-parts-pp/src/master/latex/supp/machine-parts-pp.pdf>

For documentation and the latest stable release of the planted partition generator, please visit <https://pypi.org/project/machine-parts-pp/>

The data files can be found in the following bitbucket folder:
<https://bitbucket.org/Bakkelund/machine-parts-pp/src/master/data/>

---

**Authors:**

-  Daniel Bakkelund (<daniel.bakkelund@ifi.uio.no>)

