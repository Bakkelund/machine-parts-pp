\documentclass[11pt, letterpaper]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=1.5cm]{geometry}
\usepackage{titlesec}
\usepackage{tabu}
\usepackage{longtable}
\usepackage{enumitem}
\usepackage{amssymb}
\usepackage{xcolor}
\newlist{selectlist}{itemize}{2}
\setlist[selectlist]{label=$\square$,leftmargin=*,noitemsep,topsep=0pt}

\usepackage{lmodern}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=blue,
}
 
\urlstyle{same}

% Set up the section label formatting
\titleformat{\section}[block]{\hspace{1em}\bfseries}{\thesection.}{0.5em}{} 
\titleformat{\subsection}[block]{\hspace{1em}}{\thesubsection}{0.5em}{}

\begin{document}
% Create the title block
\begin{flushleft}



{\Huge \bf {\it Data in Brief} article template} \textbf{Version 10.1 (October 2021)}\\
\vskip 0.5cm
{\bf Before you complete this template, a few important points to note:}

%% main text
\begin{itemize}
\item[$\bullet$]There are two ways to submit a data article to {\it Data in Brief}:
\begin{itemize}
\item[$\circ$]{\bf Directly to the journal}: If you choose this route, you can skip the supporting article instructions on the following page and move directly to the \underline{data article template} on page 3.
\vskip 0.5cm
\item[$\circ$]{\bf Alongside a research article that you are submitting to a separate Elsevier journal}: Please read the \underline{supporting article instructions} on page 2 before you complete this template.
\end{itemize}
\item[$\bullet$]Whichever submission option you choose, it’s important that you double check your manuscript {\bf meets the data article criteria} on our webpage: \href{https://www.journals.elsevier.com/data-in-brief/policies-and-guidelines/what-data-are-suitable-for-data-in-brief}{\underline{What Data are Suitable for Data in Brief}}. 
\item[$\bullet$]The format of a data article is different from that of a traditional research article. To help you write yours, we have created the template on page 3. {\bf We will only consider data articles submitted using this template.}
\item[$\bullet$]It is mandatory to {\bf publicly share the research data} referred to in your {\it Data in Brief} article (you will find information on our data sharing criteria in the template).  
\item[$\bullet$]Please consult the {\it Data in Brief} \href{https://www.elsevier.com/journals/data-in-brief/2352-3409/guide-for-authors}{\underline{Guide for Authors}} when preparing your manuscript; it highlights mandatory requirements and is packed with useful advice. We have also developed a \href{https://www.journals.elsevier.com/data-in-brief/about-data-in-brief/video-how-to-submit-your-research-data-article-to-data-in-brief}{\underline{step-by-step video guide}} to help you complete this template accurately and increase your chances of acceptance. 
\end{itemize}

\textbf{Still got questions?} \\
\begin{itemize}
\item[$\circ$]\href{https://www.journals.elsevier.com/data-in-brief/about-data-in-brief/data-in-brief-faq}{\underline{Visit our FAQ page}} 
\item[$\circ$]Email our Managing Editors at \href{mailto:dib-me@elsevier.com}{\underline{dib-me@elsevier.com}} 
\end{itemize}

\newpage
{\huge\textbf{Supporting article instructions}} 
\vskip 0.5cm
Data articles support original research. They improve research integrity and reproducibility by making the data available and discoverable and reduce unwanted duplication of research.  A supporting data article can be submitted to the same journal as your original research article.  Alternatively, a supporting article can be submitted directly to {\it Data in Brief} following acceptance of your original research. 
\vskip 0.5cm
\textbf{I want to submit my supporting data article at the same time as my original research article}:  you can find out whether your chosen Elsevier journal offers submission of supporting data articles by viewing its Guide for Authors.  In addition, any revision letter/email you receive from a participating journal will contain an offer to submit a data article to {\it Data in Brief}.
\vskip 0.5cm
Submission is completed in two steps: 
\begin{itemize}
\item[1.]{	Zip your completed data article, and all other files relevant to your {\it Data in Brief} submission (including any supplementary data files), into a single .zip file.}
\item[2.]{	Upload this .zip file as a “Data in Brief”-labelled item to the editorial system of the journal you are submitting your original research to.} 
\end{itemize}
\vskip 0.5cm
The .zip file is automatically transferred to {\it Data in Brief} when your research article is accepted for publication in the other journal. And if your {\it Data in Brief} article is also published, the two will link to each other on ScienceDirect.
\vskip 0.5cm
\textbf{I want to submit my supporting data article following acceptance of my original research article}:  in this case, you can submit directly to {\it Data in Brief's}  submission system.  Please note that the article template asks for the details of your accepted manuscript so that we can link your original research article with your supporting data article.
\vskip 0.5cm
\textbf{Please use the article template to write your data article}:  regardless of when you choose to submit your supporting article, please use the article template on page 3.
\vskip 0.5cm
\begin{itemize}
\item[$\bullet$]Step 1 – Write\\
Fill in the easy-to-use template provided on page 3. The template shows you exactly what reviewers are looking for in your data article.
\item[$\bullet$]Step 2 – Share\\
Make your data accessible by including it in the article and by posting them to one of the repositories we support, such as Mendeley Data.
\item[$\bullet$]Step 3 – Submit\\
Submit directly to Data in Brief or together with your original research article.
\end{itemize}

\clearpage
\hypertarget{target2}{}
{\huge\textbf{Data Article template}} \\
\vskip 0.5cm
\noindent
Please fill in the template below. As you complete each section, it’s important to carefully read the associated instructions, which you will find in italicised text. 

\begin{center}
\colorbox{yellow}{\textbf{ \textit{ Please delete this line and everything above it before submitting your article.} }}
\colorbox{yellow}{\textbf{ \textit{ In addition, we ask you to delete the instructions, including those featured in the }}} 
\colorbox{yellow}{\textbf{ \textit{ Article Information and Specifications table.}}}
\colorbox{yellow}{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - }
\end{center}

\textbf{Article information}\\
\vskip 0.5cm
\textbf{Article title}\\ \textit{The article title \underline{must} include the word ‘data’ or ‘dataset’.  Please avoid acronyms and abbreviations where possible. Where your data article supports an original research article, the title should differ from the title used in your research paper.}
\vskip 0.5cm
\textbf{Authors}\\ \textit{List all authors. Please mark the corresponding author with (*)}
\vskip 0.5cm
%Insert Affiliations
\textbf{Affiliations}\\ \textit{Please include the full address of each author institution}
\vskip 0.5cm
%Insert Contact Email
%Include institutional email address of the corresponding author
\textbf{Corresponding author’s email address and Twitter handle}\\ \textit{Institutional email address preferred. \\ If you have a Twitter handle, please add it here ‘twitter: @....’}
\vskip 0.5cm
%Insert Keywords
\textbf{Keywords}\\ \textit{Include 4-8 keywords (or phrases) to help others discover your article online. Avoid repeating words used in your title.}
\vskip 0.5cm
%Insert Abstract
\textbf{Abstract}\\ \textit{The abstract should describe the data collection process, the analysis performed, the data, and their reuse potential. It should \underline{not} provide conclusions or interpretive insights. Minimum length 100 words / maximum length 500 words. If your article is being submitted via another Elsevier journal as a co-submission, please cite that research article in the abstract.}  
\vskip 0.5cm
\textit{\textbf{Tip:} Do not use words such as ‘study’, ‘results’ and ‘conclusions’ – a data article should only be describing your data.}
\vskip 0.5cm
\textbf{Specifications table}\\
\vskip 0.2cm 
\textit{Every section of this table is mandatory. Please enter information in the right-hand column and remove all the instructions in grey, italicised text.}
%
%\vskip6pt\noindent
%{\small\textbf{\textit{Please delete this line and everything above it before submitting your
%article, in addition to anything in [square brackets] below, including
%in the Specifications Table}}\vskip6pt\hrule\vskip12pt}
%
%{\fontsize{7.5pt}{9pt}\selectfont
%%%%
%\noindent\textbf{Specifications Table} 
%
%Every section of this table is mandatory. 
%Please enter information in the right-hand column and remove all the instructions
\begin{longtable}{|p{33mm}|p{124mm}|}
%\hline
%\endhead
\hline
%\endfoot
\textbf{Subject}                & \textit{Please select Subject Area from the list available at: 
                         \href{https://www.elsevier.com/__data/assets/excel_doc/0012/736977/%
                               DIB-categories.xlsx}{DIB categories}.}\\
\hline                         
\textbf{Specific subject area}  & \textit{Briefly describe the specific subject area. Max 150 characters.}\\
\hline
\textbf{Type of data}           & \textit{We want to understand the type(s) of data this article describes. Delete any descriptions from the list that don’t apply. If your data type isn’t featured, please manually add it.}
\vskip 0.4cm                         
                         Table\newline
                         Image\newline
                         Chart\newline
                         Graph\newline
                         Figure
\vskip 0.4cm
\\                                   
\hline
\textbf{How the data were acquired} & \textit{State how the data were acquired: e.g., via microscope, SEM, NMR, mass spectrometry, etc. If it was via a survey, you must submit a copy of that survey as supplementary material. If the survey is not written in English, please provide an English-language translation.}  
\vskip 0.5cm
\textit{Please also provide information on the Instruments used to acquire your data (e.g., hardware, software, program), including any relevant make and model details.} 
\vskip 0.2cm
\\
\hline                         
\textbf{Data format}            & \textit{List your data format(s). Please submit all primary raw data alongside your data article or provide a link to a repository where it is hosted. Delete any descriptions from the list that do not apply. If your data format is not featured, please manually add it.}
\vskip 0.4cm
                         Raw\newline
                         Analyzed\newline
                         Filtered
\vskip 0.4cm
\\                                                    
\hline
\textbf{Description of          
data collection}             & \textit{Provide a brief description of the conditions/parameters/samples that were used for data collection. Describe the factors/samples under study that were used to generate data points and inclusion/exclusion criteria (if necessary). You may also describe how the data were normalised. Max 400 characters. }
\vskip 0.2cm
\\                         
\hline                         
\textbf{Data source location}   & \textit{Provide the information requested below. If any of the bullet points don’t apply, please delete them:}
\vskip 0.5cm
\noindent
\textit{$\bullet$ Institution:\newline
                        $\bullet$ City/Town/Region:\newline
                        $\bullet$ Country:\newline
                        $\bullet$ Latitude and longitude (and GPS coordinates, if possible) for collected} samples/data:\newline


                         \textit{If you are describing \textbf{secondary data}, this is where you list the raw data sources used.}.\\
\hline                         
\hypertarget{target1}
{\textbf{Data accessibility}}   & \textit{\textbf{All data referred to in your data article must be made publicly available prior to publication.}} 
\vskip 0.3cm
%\begin{itemize}
                         $\bullet$ \textit{If you have \textbf{raw data} linked to charts, graphs or figures in your manuscript, you can either share them with the data article (e.g., as a supplementary file) or host them on a trusted data repository.}\newline
                         
                         $\bullet$ \textit{If you are describing \textbf{secondary data}, you are required to provide a list of the primary data sources used \underline{and} make the full secondary dataset publicly available, either with the data article (e.g., as a supplementary file) or via a trusted data repository.}
%\end{itemize}
\newline 
\textit{Although we allow supplementary files, we recommend that you deposit your data in a trusted data repository ($>70$\% of Data in Brief authors now do this). View our supported data repositories on \href{https://fairsharing.org/bsg-p000100/}{FAIRsharing.org}. If you are unsure which repository to choose, we suggest \href{https://data.mendeley.com/}{Mendeley Data}.}
\vskip 0.5cm
\textit{Please indicate here where your data are hosted. If you’ve submitted them ‘With the article’ then leave that text in place below and remove the three rows requesting public repository information. If your data are in a public repository, then delete ‘With the article’ and complete the three public repository rows.} \newline

\textit{With the article}\newline

         		Repository name:\newline
                         Data identification number:\newline
                         Direct URL to data: \textit{e.g. https://www.data.edu.com - the URL should be working at the time of submission}\newline

\textit{For data that require access controls for ethical reasons (i.e., patient data), please describe how readers can request access. You must also provide a link to any Data Use Agreement (DUA) or upload a copy as a supplementary file. }\newline

                         Instructions for accessing these data:
\vskip 0.2cm
                         \textit{\textbf{Important}: if your data do have access controls, you must provide a mechanism that allows our Editors and reviewers to access the data without compromising their anonymity. Please include these instructions with your submission. Contact the Managing Editors (\href{mailto:dib-me@elsevier.com}{\underline{dib-me@elsevier.com}}) if you have any questions.}\\                         
\hline                         
\textbf{Related                 
research\newline
article}                & \textit{If your data article supports an original research article, please cite the associated research article here. You should only list \underline{one} article.}\newline

\textit{\textbf{For a published article:}}

                         \textit{J. van der Geer, J.A.J. Hanraads, R.A. Lupton, The art of writing a scientific article,J. Sci. Commun. 163 (2010) 51-59. \underline{https://doi.org/10.1016/j.Sc.2010.00372}}\newline

\textit{\textbf{For an article which has been accepted and is in press:}}

                         \textit{J. van der Geer, J.A.J. Hanraads, R.A. Lupton, The art of writing a 
                         scientific article, J. Sci. Commun. In Press.}\newline

                         \textit{If your data article is \textbf{not related to a research article,} please delete this entire ‘Related research article’ row}\\
\hline                         
\end{longtable}

%%%            

\textbf{Value of the Data}\\
\textit{Provide up to 6 bullet points explaining why these data are of value to the scientific community. At a minimum you must answer the first 3 bullet point questions below (then those questions should be deleted). Please keep your points brief – ideally, each one should be no longer than 400 characters. Avoid any conclusions or inferences.
\begin{itemize}
\itemsep=0pt
\parsep=0pt
\item[$\bullet$]Why are these data useful? 
\item[$\bullet$]Who can benefit from these data?
\item[$\bullet$]How can these data be used/reused for further insights and/or development of experiments?
\end{itemize}}
\vskip 0.5cm

\textbf{Data Description}\\
\textit{Individually describe the data files that appear in this article (not the article it supports) e.g., figure 1, figure 2, table 1, dataset, raw data, supplementary data, etc. Please ensure you refer to every file separately and provide a clear description for each one (do not just list them). Legends should be included for tables, figures and graphs. No insights, interpretations, background, or conclusions should appear in this section. } 

\noindent
\textit{\textbf{Tip:} do not forget to describe any supplementary data files.}
\vskip 0.5cm

\textbf{Experimental design, materials and methods}\\

\textit{Provide a complete description of the experimental design and methods used to acquire these data. Include any programs or code files used for data filtering or analysis. It is important that this section is as comprehensive as possible. If this is a co-submission, this section should contain more detail than the corresponding section of your accompanying research article, and it should be written in original text (i.e. do not copy it). There is no character limit; however, no insights, interpretations, or background should be included.}

\noindent
\textit{\textbf{Tip:} do not describe your data (figures, tables, etc.) here - you should have already done that in the \textbf {Data description} section above.}
\vskip 0.5cm

\textbf{Ethics statements}\\
\noindent 
\textit{Data in Brief’s \href{https://www.elsevier.com/journals/data-in-brief/2352-3409/guide-for-authors}{Guide for Authors} contains detailed information on the ethical guidelines that all authors must comply with. In addition, we ask you to complete the relevant statement(s) below.  Please delete those which are not relevant for your data.}

\noindent
\textit{\textbf{If your work involved human subjects,} please include a statement here confirming that the relevant informed consent was obtained from those subjects:}

\textit{\textbf{If your work involved animal experiments:} please include a statement here confirming that those experiments complied with the ARRIVE guidelines and were carried out in accordance with the U.K. Animals (Scientific Procedures) Act, 1986 and associated guidelines; EU Directive 2010/63/EU for animal experiments; or the National Institutes of Health guide for the care and use of laboratory animals (NIH Publications No. 8023, revised 1978). Note, the sex of the animals must be indicated, and, where appropriate, the influence (or association) of sex on the results of the study:}

\textit{\textbf{If your work involved data collected from social media platforms,} please include a statement here confirming that a) informed consent was obtained from participants or that participant data has been fully anonymized, and b) the platform(s)’ data redistribution policies were complied with: }
\vskip0.5cm
\noindent
\textbf{CRediT author statement}\\
\noindent
{\it CRediT is in initiative that enables authors to share an accurate and detailed description of their diverse contributions to a published work.}\\

\noindent
{\it Example of a CRediT author statement: 
\textbf{Zhang San}: Conceptualization, Methodology, Software \textbf{Priya Singh}: Data curation, Writing- Original draft preparation. \textbf{Wang Wu}: Visualization, Investigation. \textbf{Jan Jansen}: Supervision. \textbf{Ajay Kumar}: Software, Validation.: \textbf{Sun Qi}: Writing- Reviewing and Editing.}\\

\noindent
{\it Please add a CRediT author statement for your data article here, using the \href{https://www.elsevier.com/authors/journal-authors/policies-and-ethics/credit-author-statement}{categories listed on this webpage}.}\\
\vskip0.5cm

\textbf{Acknowledgments}\\
\textit{All contributors who do not meet the criteria for authorship should be listed in an acknowledgments section. 
\vskip0.3cm
In addition, please list any funding sources in this section. List funding sources in this standard way to facilitate compliance to funder's requirements:
\vskip0.3cm
Funding: This work was supported by the National Institutes of Health [grant numbers xxxx, yyyy]; the Bill \& Melinda Gates Foundation, Seattle, WA [grant number zzzz]; and the United States Institutes of Peace [grant number aaaa].
\vskip0.3cm
It is not necessary to include detailed descriptions on the program or type of grants and awards. When funding is from a block grant or other resources available to a university, college, or other research institution, submit the name of the institute or organization that provided the funding.
\vskip0.3cm
If no funding has been provided for the research, please include the following sentence:
\vskip0.3cm
This research did not receive any specific grant from funding agencies in the public, commercial, or not-for-profit sectors.}\newline

\textbf{Declaration of Competing Interest}\\

\textit{Please \textbf{tick} the appropriate statement below and declare any financial interests/personal relationships which may affect your work in the box below.}
\vskip0.3cm
\begin{itemize}
\item[$\square$]{The authors declare that they have no known competing financial interests or personal relationships that could have appeared to influence the work reported in this paper.}

\item[$\square$]{The authors declare the following financial interests/personal relationships which may be considered as potential competing interests: }
\end{itemize}
\vskip0.3cm
\textit{Please declare any financial interests/personal relationships which may be considered as potential competing interests here.}\newline

\newpage
\textbf{References}\\

\noindent 
\textit{References are limited to a maximum of 20 and excessive self-citation is not allowed. Please cite any article you have referred to while collecting or analysing data and preparing your manuscript. \textbf{If your data article supports an original research article which is published or in press, please cite the associated article here; ideally, it should be the first citation. } 
\vskip0.2cm
Please review the References section in the \href{https://www.elsevier.com/journals/data-in-brief/2352-3409/guide-for-authors}{Guide for Authors} for a comprehensive guide to Data in Brief reference style and then list your references below, numbering each one as you go.}

\begin{center}
---------------------------------------------------------------------------------------------------------------------------
\end{center}

\vskip 1.5cm
\begin{center}
{\huge \it  Reminder: Before you submit, please delete all the instructional text in italicised text, wherever it appears – including here. Thank you!
\vskip 1.0cm
 And finally, we welcome your feedback on how we can improve this template. \href{https://forms.office.com/Pages/ResponsePage.aspx?id=P-50kiWUCUGif5-xXBBnXTeXkbO343VFrbpYVBvxdZtUM05UVjIwM0U4WlRKUldCOTNMRUQwOVRHTy4u}{\underline{Click here}} to provide anonymous feedback via a very short survey.}
\end{center}

\end{flushleft}
\end{document}


