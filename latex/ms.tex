\documentclass[11pt, letterpaper]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=1.5cm]{geometry}
\usepackage{titlesec}
\usepackage{tabu}
\usepackage{longtable}
\usepackage{enumitem}
\usepackage{amssymb}
\usepackage[dvipsnames]{xcolor}
\selectcolormodel{rgb}
\newlist{selectlist}{itemize}{2}
\setlist[selectlist]{label=$\square$,leftmargin=*,noitemsep,topsep=0pt}

\usepackage{lmodern}


% Non-template packages and commands
\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{compat=1.3}
\usepackage{caption}
\usepackage[version=4]{mhchem}

\usepackage{soul}

\makeatletter
\newdimen\SOUL@dimen %new
\def\SOUL@ulunderline#1{{%
    \setbox\z@\hbox{#1}%
    %\dimen@=\wd\z@
    \SOUL@dimen=\wd\z@ %new
    \dimen@i=\SOUL@uloverlap
    \advance\SOUL@dimen2\dimen@i %\dimen@ exchanged too
    \rlap{%
        \null
        \kern-\dimen@i
        %\SOUL@ulcolor{\SOUL@ulleaders\hskip\dimen@}%
        \SOUL@ulcolor{\SOUL@ulleaders\hskip\SOUL@dimen}% new
    }%
    \unhcopy\z@
}}

% Back to template packages 
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=blue,
}
 
\urlstyle{same}

% Set up the section label formatting
\titleformat{\section}[block]{\hspace{1em}\bfseries}{\thesection.}{0.5em}{} 
\titleformat{\subsection}[block]{\hspace{1em}}{\thesubsection}{0.5em}{}

\definecolor{darkGreen}{rgb}{0.0, 0.5, 0.0}

\newcommand{\deft}[1]{{\textbf{\boldmath{#1}}}}
\newcommand{\la}{{$\langle$}}
\newcommand{\ra}{{$\rangle$}}

\begin{document}
 
\textbf{Article information}\\
\vskip 0.5cm

\textbf{Article title}\\
Machine part data with part-of relations and part dissimilarities for planted partition generation

\vskip 0.5cm
\textbf{Authors}\\
Daniel Bakkelund$^*$ 

\vskip 0.5cm
\textbf{Affiliations}\\
Institute of Informatics, University of Oslo \\
TechnipFMC

%Insert Contact Email
%Include institutional email address of the corresponding author
\vskip 0.5cm
\textbf{Corresponding author’s email address and Twitter handle}\\
daniel.bakkelund@ifi.uio.no 

\vskip 0.5cm
\textbf{Corresponding author’s postal address}\\
PO Box $1080$, $0316$ Oslo, Norway 

%Insert Keywords
\vskip 0.5cm
\textbf{Keywords}\\

Machine parts, part-of relations, dissimilarity, planted partition, clustering, link recovery


%Insert Abstract
\vskip 0.5cm
\textbf{Abstract}\\
Identifying relationships between entities in data is a central topic across various
industries and businesses, from social networks to supply chain and heavy manufacturing industries.
%
In this paper we present data from a database of machinery represented in terms of machine parts.
The machine parts are originally organised in tree structures where the vertices are
machine part types, and the edges are ``part-of'' relations. Hence, each tree represents
a type of machinery broken down into its machine part constituent types.
%
The data we present is the union over these trees, making up a directed acyclic graph
describing the type hierarchy of the machine parts.

The motivation for publishing the dataset is the following real-world industry problem:
Each tree represents a mechanical design, and over time some designs have been copy-pasted with
minor modifications. The new instances have been given new identifiers with no reference to where
from they were copied.
%
In hindsight, it is desirable to recover the copy-paste links to for interchange between
essentially identical designs. However, telling which parts are
copies of which other parts has turned out to be difficult. In particular, the metadata has a tendency
of displaying higher similarities within a composite part than between a part and its copy.
%
Due to non-disclosure, we cannot provide the metadata, but we provide element wise dissimilarities
that are generated based on the metadata using classical methods such as Jaccard similarity on description
texts, material types etc.
The dissimilarities are obtained from a data science project in the company owning the data, trying to
tackle the very problem of recovering the copy-paste links.

Availability of labeled data on this data set is limited, so
based on our in-depth knowledge of the problem domain, we present a data synthetisation method
that can generate arbitrarily large problem instances of the copy-paste problem based on the sample data,
that provides a realistic representation of the real world problem.
The problems are presented as planted partitions of vertices of directed acyclic graphs
with vertex dissimilarities, and thus constitutes a typical classification problem along the lines of
graph- or network clustering.

The type of industry data we present is usually company confidential, bound by intellectual
property rights, and generally not available to scientists.
We therefore publish this anonymised dataset to offer real world sample data and
generated problem instances for researchers that are interested in this type of classification problems,
and on which theories and algorithms can be tested.

The data and the problem generation methodology are backed by a Python implementation, providing both
data access and an API for parameterised problem generation. The data is also available as raw files.

\vskip 0.5cm

\textbf{Specifications table}\\
\vskip 0.2cm
\begin{longtable}{|p{33mm}|p{124mm}|}
\hline
\textbf{Subject} &
Data Science \\
\hline                         
\textbf{Specific subject area} &
Applied Machine Learning  \\
\hline
\textbf{Type of data} &
Table of machine parts types and part-of relations \newline
Table of dissimilarities between machine parts types \newline
Python code \\
\hline
\textbf{How the data were acquired} &
Extraction from company database.
\\
\hline                         
\textbf{Data format} &
Analysed \newline
Raw
\vskip 0.4cm
\\                                                    
\hline
\textbf{Description of data collection} &
\textbf{Machine part types:}
The machine part data is extracted from a relational database.
The initial raw data is organised as trees of machine parts, where each node has a unique identifier and
a type-id. In the data collection process, the trees have been replaced by a graph of types as follows.
The vertices of the graph corresponds to the set of types of the tree vertices. Then, edges are added
to the graph if there is an edge in a tree between vertices of corresponding types. The resulting graph
thus represents the \emph{type part-of structure} defined by the trees.
%
Since one type of machinery may be a part of different types of high level machinery, in the way a type
of tire may be a part of many types of cars, the type hierarchy becomes a graph, rather than a tree.
And moreover, since a part cannot another part of the same type~\cite{Rescher1955},
the type hierarchy is a directed acyclic graph.\footnote{If a part contains another part of the
  same type as a sub-part, this leads to a cycle of containment.
  Since this leads to an infinitely deep structure, it is not achievable for practical manufacturing.} 

Our particular subset of machine part types was chosen as follows. When we generated the above graph
for all machine parts, we found that the graph had one very large connected component and a large set of
disconnected vertices, but also eight connected component in the range of $11$ to $40$ vertices.
Since each of these connected component closely correspond to single designs, we chose these eight
connected components as our sample dataset.

%\vskip 0.4cm

\textbf{Dissimilarities:}
The dissimilarity data is obtained from an internal project in the company owning the data trying to tackle
the very problem of recovering the mentioned copy-paste links.
The dissimilarities are generated based on metadata about the machine part types, such as
description texts, material types, weights etc. 
\\                         
\hline                         
\textbf{Data source location}   & 
Proprietary database owned by TechnipFMC\footnote{\url{https://www.technipfmc.com/}}, a privately held
company in the Oil \&{} Gas sector.
\\
\hline                         
\hypertarget{target1}
{\textbf{Data accessibility}}   &
%            
Repository name: Mendeley Data \newline
Data identification number: \href{https://doi.org/10.17632/dhhxzdzm3v.1}{10.17632/dhhxzdzm3v.1} \newline
Direct URL to data: \newline
\url{https://data.mendeley.com/v1/datasets/dhhxzdzm3v/} \\
\hline                         
%
\textbf{Related research \newline article}  &
% \textit{J. van der Geer, J.A.J. Hanraads, R.A. Lupton, The art of writing a scientifi
%c article,J. Sci. Commun. 163 (2010) 51-59. \underline{https://doi.org/10.1016/j.Sc.2010.00372}}\newline
\textit{Daniel Bakkelund, Order Preserving Agglomerative Hierarchical Clustering, Mach Learn. (2021).
  \underline{\url{https://doi.org/10.1007/s10994-021-06125-0}}.} \\
\hline                         
\end{longtable}

\textbf{Value of the Data}\\
\begin{itemize}
  \itemsep=0pt
  \parsep=0pt
\item[$\bullet$] The type of data we present is usually company confidential, and therefore very difficult to
  come by for researchers. By publishing a small subset of the data together with code that can
  proliferate the data based on our understanding of the problem domain, we hope to allow other researchers
  to test their hypotheses and methods on close to real world data.

  In this respect, we particularly mention the problem of
  \emph{order preserving clustering}~\cite{Bakkelund2021}.
  This is a field in development where there are currently no public datasets available for benchmarking
  and/or testing of methods and hypotheses.
  We therefore wish to publish this dataset, and the model for generating classification problems from
  this dataset, to support further development of this new branch of classification research.
  %
\item[$\bullet$] Since we present dissimilarity data with additional relations, the main audience is likely
  to be researchers and practitioners within classification and clustering that work with data that has
  additional structure.
  As a non-exhaustive list of examples we mention
  graph- and network clustering~\cite{Malliaros2013},
  order preserving clustering~\cite{GhoshdastidarPerrotLuxburg2019},
  acyclic partitioning~\cite{HerrmannEtAl2017}
  and
  clustering with constraints~\cite{BasuDavidsonWagstaff2008}.
  %
\item[$\bullet$] One of the contributions of this paper is a
  model for generation of planted partitions simulating the copy-paste problem. The model is based
  on our in-depth knowledge about the problem domain, and we believe that this model, together with the
  published data, provides realistic representations of the previously described copy-paste problem.
  Hence, models and algorithms that perform well on these
  planted partitions can be expected to perform well also on the real dataset.
  %
\item[$\bullet$] As for the mentioned industry problem, this is an \emph{excess inventory problem} in that
  the machine part manufacturer has an increasing amount of machinery in stock. A traditional approach to
  excess inventory is that of \emph{excess inventory disposal}~\cite{Rosenfield1989} to free up capacity.
  However, this is easily sub-optimal for expensive machinery, both with respect to economy, and also with
  respect to the environment, as manufacturing of complex steel based machinery has a large \ce{CO2} footprint.
  Rather, TechnipFMC states that if they can match similar machinery in the described fashion, then
  this will lead to increased sales from inventory, rather than producing new machinery.
  Thus, yielding a double up-side compared to decimating the machinery in stock.
\end{itemize}

\vskip 0.5cm
\textbf{Data Description}\\

This section describes the format of the flat files containing the machine part data and the dissimilarity
data. Note, however, that the data is also available through the provided python API.

The data is available as two
CSV\footnote{\url{https://en.wikipedia.org/wiki/Comma-separated\_values}} files, one file for the
machine part structures, and one for the machine part dissimilarities.

\textbf{The machine part file} \\
The machine part file is named {\tt parts.csv}.
Each line in the machine part file is formatted as
\begin{center}
  %\fbox{
    \la id \ra \ (,\la child id \ra)*
  %}
\end{center}
That is, each line is a comma separated list of integers. The first integer is the part type
identifier ($id$), and the remaining integers (if any) are the part types that
occur as ``part-of'' type $id$.

\bigskip

For example, if the data was constituted by the graph
\begin{center}
  \begin{tikzpicture}[yscale=0.6]
    \node[draw,circle,minimum size=0.4cm,inner sep=0cm] (a) at (0,1) {$0$};
    \node[draw,circle,minimum size=0.4cm,inner sep=0cm] (b) at (1,1) {$1$};
    \node[draw,circle,minimum size=0.4cm,inner sep=0cm] (c) at (0,0) {$3$};
    \node[draw,circle,minimum size=0.4cm,inner sep=0cm] (d) at (1,0) {$2$};
    \draw[->] (a) -- (b);
    \draw[->] (a) -- (d);
    \draw[->] (c) -- (d);
  \end{tikzpicture}
\end{center}
where the arrows indicate that $1$ and $2$ are both \emph{part-of} $0$ and that $2$ is \emph{part-of} $3$,
then the corresponding file would look like
\begin{center}
  \begin{tabular}{l}
    $0,1,2$ \\
    $1$ \\
    $2$ \\
    $3,2$ \\
  \end{tabular}
\end{center}

\bigskip

\textbf{The dissimilarities file} \\
The dissimilarities are also organised in a CSV file, {\tt dissimilarities.csv}, with each line on the format
\begin{center}
  %\fbox{
    \la integer:$a$\ra,\la integer:$b$\ra,\la decimal number:$d$\ra
  %}
\end{center}
Here, the integers are part type ids, as given in {\tt parts.csv}, and the
dissimilarity $d$ is the dissimilarity between the specified types. The indices are always ordered so that
$a < b$, and for the dissimilarities, we always have $0 \le d \le 1$.

\bigskip

Given the above example part type structure, a corresponding dissimilarity file would be on the form
\begin{center}
  \begin{tabular}{l}
    $0,1,0.2021$ \\
    $0,2,0.3141$ \\
    $0,3,0.2718$ \\
    $1,2,0.1414$ \\
    $1,3,0.7071$ \\
    $2,3,0.2600$
  \end{tabular}
\end{center}
That is, the dissimilarity between $0$ and $1$ is $0.2021$, meaning $0$ and $1$ are more dissimilar
than, say, $1$ and $2$ that has a dissimilarity of $0.1414$.

\bigskip

\textbf{Some statistics on the connected components} \\
The part data constitutes eight connected components, where each connected component is a small DAG.
Table~\ref{table:ccs-info} lists some typical graph statistics for the connected components.

\begin{center}
  \begin{tabular}{c c c c c}
    cc no. & cc size & in/out deg. & $p$ \\ % & expected ties \\
    \hline
    $0$ & $12$ & $0.92$ & $0.17$ \\ % & $2.36$ \\ 
    $1$ & $14$ & $0.93$ & $0.14$ \\ % & $4.79$ \\ 
    $2$ & $13$ & $0.92$ & $0.15$ \\ % & $2.17$ \\ 
    $3$ & $40$ & $1.27$ & $0.07$ \\ % & $8.97$ \\ 
    $4$ & $20$ & $1.35$ & $0.14$ \\ % & $3.96$ \\ 
    $5$ & $11$ & $1.18$ & $0.24$ \\ % & $2.20$ \\ 
    $6$ & $20$ & $1.10$ & $0.12$ \\ % & $4.22$ \\ 
    $7$ & $20$ & $0.95$ & $0.10$ \\ % & $3.96$ \\ 
    \hline
  \end{tabular}
  \captionof{table}{  
    \begin{minipage}[t]{.9\textwidth}
      Some key characteristics of the connected components of the machine parts dataset:
      \newline
      \begin{tabular}{l@{ -- }p{.7\textwidth}}
        cc no.         & the index of the connected component \\
        cc size        & the number of vertices in the connected component \\
        The in/out deg & the directed average degree of the connected component \\
        $p$            & the probability that for a pair of random vertices $a$ and $b$, the edge $(a,b)$
        exists in the transitive reduction
      \end{tabular}
      \newline
      \emph{Note: The table is an adaptation of~\cite[Table 2]{Bakkelund2021}.}
    \end{minipage}
  }
  \label{table:ccs-info}
\end{center}


\textbf{Parent-child dissimilarities} \newline
A feature of the dissimilarity data, is that there is a high probability for the dissimilarity of a part
and a sub-part to be low. This is due to a significant overlap in metadata, stemming from the fact that
a part and its sub-parts are often closely related in several ways.
For example, machinery wrought out of steel will often
have parts with similar material- and mechanical properties. For machinery that will be used under harsh
environmental conditions, the environmental characteristics of the parts must necessarily be very similar.
Description texts describing a sub-part will often contain references to the containing part, and so on.
%As an example, consider a tire
%that has an air valve as a part. The two parts will often be made of similar material, used in similar
%conditions (mud and snow), have the same air pressure capacities etc.
Deducing the dissimilarities based
on this metadata therefore sometimes lead to low dissimilarity between parent and child.
For the copy paste problem,
this is a complicating factor, since a part and a sub-part can never be copy-paste related.
The dissimilarity distributions between parts and sub-parts are displayed in Figure~\ref{fig:dissim-dist}.

\begin{center}
  \begin{tikzpicture}
    \begin{axis}[
        yscale=0.5,
        %title = {Parent-child dissimilarity CDF},
        xlabel = {$x$},
        ylabel = {$\scriptstyle \Pr\!\big(d(p,c) \, < \, x \big)$},
        ymin = 0,
        ymax = 1,
        xmin=0,
        xmax=1,
        ytick={0,0.5,1.0},
        yticklabels={$0$,$\frac{1}{2}$,$1$},
        minor y tick num = 4,
        legend style={at={(0,2)},anchor=north west},
      ]  
      \addplot[] coordinates {
        (0.0000,0.0000) (0.0526,0.0123) (0.1053,0.0675) (0.1579,0.1350) (0.2105,0.1534) (0.2632,0.1840)
        (0.3158,0.2147) (0.3684,0.2515) (0.4211,0.2577) (0.4737,0.2638) (0.5263,0.2945) (0.5789,0.3129)
        (0.6316,0.3436) (0.6842,0.3988) (0.7368,0.4417) (0.7895,0.5276) (0.8421,0.5521) (0.8947,0.5644)
        (0.9474,0.6135) (1.0000,1.0000)};
      \addlegendentry{$1$ step$\phantom{s}$}
      %
      \addplot[dashed] coordinates {
        (0.0000,0.0068) (0.0526,0.0068) (0.1053,0.0068) (0.1579,0.0408) (0.2105,0.0748) (0.2632,0.0816)
        (0.3158,0.1020) (0.3684,0.1020) (0.4211,0.1156) (0.4737,0.1293) (0.5263,0.1429) (0.5789,0.1633)
        (0.6316,0.1769) (0.6842,0.2109) (0.7368,0.2449) (0.7895,0.2925) (0.8421,0.3197) (0.8947,0.3265)
        (0.9474,0.3401) (1.0000,1.0000)};
      \addlegendentry{$2$ steps}
      %
      \addplot[dotted,thick] coordinates {
        (0.0000,0.0000) (0.0526,0.0000) (0.1053,0.0088) (0.1579,0.0088) (0.2105,0.0175) (0.2632,0.0263)
        (0.3158,0.0351) (0.3684,0.0351) (0.4211,0.0351) (0.4737,0.0526) (0.5263,0.0614) (0.5789,0.0614)
        (0.6316,0.0877) (0.6842,0.1140) (0.7368,0.1316) (0.7895,0.1491) (0.8421,0.1754) (0.8947,0.1754)
        (0.9474,0.2456) (1.0000,1.0000) }; 
      \addlegendentry{$3$ steps}
    \end{axis}
  \end{tikzpicture}
  \captionof{figure}{
    Probability for a part and a sub-part to have dissimilarity no higher than $x$.
    The first axis value is the dissimilarity in the range $[0,1]$, and the second
    axis is the probability of a parent part $p$ and child part $c$ to have a dissimilarity
    $d(p,c)$ no higher than $x$; that is, $\Pr\!\big(d(p,c) \, < \, x \big)$.
    The curves represent parent-child pairs that are separated by $1$, $2$ or $3$ levels.
    We see that there is a
    higher probability for low dissimilarity between a part and a contained part if the containment
    is direct ($1$ step) compared to a nested containment ($\ge 2$ steps).
  }
  \label{fig:dissim-dist}
\end{center}

\vskip 0.5cm
\textbf{Experimental design, materials and methods}\\

In this section, we describe the model for generation of planted partitions based on the published
dataset. The model is a simplified representation of the copy-paste mechanism in the
problem domain. An important goal of the model has been to keep it simple, while at the same time not
underplaying the complexity of the copy-paste process and the following changes to the data.

It should be noted that our notion of a planted partition is not the same as the probabilistic
concept of planted partitions sometimes encountered in clustering literature~\cite{Mossel2015}.
Rather, in the model we present, the generation of the planted partitions is based on our understanding of
the copy-paste problem, and our wish to simulate this. 
We still choose to refer to this as \emph{planted partitions}, since they are, in name, exactly that.

\medskip

On a high level, the model works as follows. Given a connected component $C$ from {\tt parts.csv},
the dissimilarities from {\tt dissimliarities.csv}, a positive integer $n$,
a location parameter $\mu$ and a scale parameter $\sigma^2$,
we generate a planted partition with $n+1$ parallel instances through the following steps:
\begin{enumerate}
\item Make $n$ copies of $C$, providing us with the connected components $\{C_i\}_{i=0}^n$
  where $C=C_0$. Denote the vertices of $C$ by $\{v_1^0,\ldots,v_m^0\}$, and similarly denote the
  vertices of $C_i$ by $\{v_1^i,\ldots,v_m^i\}$ so that $v_k^i$ is the copy in $C_i$ of $v_k^0$.
\item For every connected component $C_i$, define the \emph{intra component dissimilarities}
  as follows:
  \[
  d(v_r^i,v_s^i) = d_0(v_r^0,v_s^0),
  \]
  where $d_0$ is the dissimilarity found in {\tt dissimilarities.csv}.
  That is, the intra component dissimilarities in the copies are the same as in the original
  connected component.
\item Let $Y \sim \mathcal{N}(\mu,\sigma^2)$ be a random variable where $\mathcal{N}(\mu,\sigma^2)$ is the
  Gaussian distribution located at $\mu$ with variance $\sigma^2$.
  We define the stochastic function $\alpha : [0,1] \to [0,1]$
  by $\alpha(x) = x + Y$ through rejection sampling, naively continuing to draw from $Y$
  until $x + Y \in [0,1]$.
  The \emph{inter component dissimilarities} may now be defined as
  \[
  d(v_r^i,v_s^j) =  \alpha \Big( d_0(v_r^0,v_s^0) \Big).
  \]
  That is, we distort the dissimilarity between the copy-paste instances by adding Gaussian noise.
\end{enumerate}

The result is a set of machine parts with part-of relations that is the union of all the copies $C_i$
equipped with dissimilarities. The corresponding planted partitions are the sets
$P_k = \{x_k^i\}_{i=0}^n$ for $1 \le k \le m$, defining the $m$ sets of copy-paste elements.

An example is depicted in Figure~\ref{fig:copy}.

\begin{figure}[htpb]
  \begin{center}
    \begin{tikzpicture}[yscale=0.8,xscale=1.3]
      \node[draw,circle,minimum size=0.4cm,inner sep=0cm] (a0) at (0,1) {$0$};
      \node[draw,circle,minimum size=0.4cm,inner sep=0cm] (b0) at (1,1) {$1$};
      \node[draw,circle,minimum size=0.4cm,inner sep=0cm] (c0) at (0,0) {$3$};
      \node[draw,circle,minimum size=0.4cm,inner sep=0cm] (d0) at (1,0) {$2$};
      \draw[->] (a0) -- (b0);
      \draw[->] (a0) -- (d0);
      \draw[->] (c0) -- (d0);
      %
      \node[draw,circle,minimum size=0.4cm,inner sep=0cm] (a1) at (2,1) {$0'$};
      \node[draw,circle,minimum size=0.4cm,inner sep=0cm] (b1) at (3,1) {$1'$};
      \node[draw,circle,minimum size=0.4cm,inner sep=0cm] (c1) at (2,0) {$3'$};
      \node[draw,circle,minimum size=0.4cm,inner sep=0cm] (d1) at (3,0) {$2'$};
      \draw[->] (a1) -- (b1);
      \draw[->] (a1) -- (d1);
      \draw[->] (c1) -- (d1);
      %
      \draw[dashed,thick] (a0) to[in=130,out=50] (b0);
      \node (L1) at (0.8,2.5) {$\scriptstyle d_0(0,1)$};
      \draw[thin] (0.8,2.3) -- (0.7,1.4);
      %
      \draw[dashed,thick] (a0) to[in=130,out=60] (b1);
      \node (L2) at (2.8,2.5) {$\scriptstyle \alpha(d_0(0,1))$};
      \draw[thin] (2.8,2.2) -- (2.3,1.7);
      %
      \draw[dashed,thick] (d0) to[in=-120,out=-50] (d1);
      \node (L3) at (0.8,-1.3) {$\scriptstyle \alpha(0)$};
      \draw[thin] (0.8,-1.0) -- (1.5,-0.5);
      %
      \draw[dashed,thick] (c1) to[in=-130,out=-60] (d1);
      \node (L4) at (2.0,-1.3) {$\scriptstyle d_0(3,2)$};
      \draw[thin] (2.0,-1.0) -- (2.27,-0.45);
    \end{tikzpicture}
    \caption{The dashed lines indicate dissimilarity links. We can see that the dissimilarity between $2$
      and the copy $2'$ is $\alpha(0)$, the dissimilarity between the element $0$ and the copy of the child
      $1'$ is $\alpha(d_0(0,1))$, which is the perturbed dissimilarity of $d_0(0,1)$. And finally, that
      the intra-component dissimilarity between $3'$ and $2'$ is identical to that in the original connected
      component, namely $d_0(3,2)$.}
    \label{fig:copy}
  \end{center}
\end{figure}


\bigskip

The model is subject to at least two simplifications that deviate from the real world case.
In both cases, we chose to do this to keep the model simple. However, we also believe that this does
not compromise the problem generation in terms of benchmarking relative the real world problem:
\begin{itemize}
\item \textit{The topology of the original and the copy is identical.} \newline
  We do not add or remove vertices or relations when we copy.
  In the real application, this happens to some extent.
\item \textit{The intra component dissimilarities are unchanged when copied.} \newline
  Since metadata is changed after copying, the intra component dissimilarities will also change
  in the real application.
\end{itemize}

\vskip 0.3cm

We summarise the input and output of the planted partition generation in Tables~\ref{table:input}
and~\ref{table:output}.

\begin{table}[tpbh]
  \begin{center}
    \begin{tabular}{l|l}
      parameter & explanation \\
      \hline
      cc-ids     & The ids of the connected components that shall be duplicated \\
      n          & The number of copies to make \\
      $\mu$      & The mean translation of the dissimilarities under $\alpha$ \\
      $\sigma^2$ & The variance in the noise applied by $\alpha$ 
    \end{tabular}
    \caption{Table of inputs to the planted partition generation process.}
    \label{table:input}
  \end{center}
\end{table}

\begin{table}[tpbh]
  \begin{center}
    \begin{tabular}{l|p{.8\textwidth}}
      data & explanation \\
      \hline
      $X$    & A set of vertices $X$ making up the union of the original connected components as
      well as all the copies \\
      $E$    & A set of edges $(a,b) \in X \times X$ denoting all the part-of relations of both the
      original connected components as well as the copies \\
      $d$    & A dissimilarity measure defined on all of $X$ generated according to the above procedure \\
      $\mathcal{P}=\{P_i\}_{i=1}^{|X|}$ &
      The planted partitions; that is, the sets consisting of machine parts that
      are copies of each other.
    \end{tabular}
    \caption{Table of outputs from the planted partition generation process.}
    \label{table:output}
  \end{center}
\end{table}

Now, given a generated problem instance $(X,E,d,\mathcal{PP})$ and a classification
procedure $\mathfrak{C}$, to which degree can $\mathfrak{C}$ recover $\mathcal{P}$ if given only
$X$, $E$ and $d$?

\vskip 0.5cm
\textbf{Python implementation}\\
An open source {\tt python} implementation of the above model is made available. The library is most easily
installed via {\tt PyPi} by
\begin{verbatim}
python3 -m pip install machine-parts-pp [--user]
\end{verbatim}
Notice that the library requires {\tt python} version $3.0$ or higher.
For further documentation of the provided functionality, please visit
\url{https://pypi.org/project/machine-parts-pp/}.

\vskip 0.5cm
\textbf{Ethics statements}\\
N/A.

\vskip 0.5cm
\textbf{CRediT author statement}\\
\textbf{Daniel Bakkelund:} Data collection and curation, methodology, software, writing. 

\noindent 
\vskip0.5cm
\textbf{Acknowledgments}\\
This work has been funded by
the Department of Informatics (The Faculty of Mathematics and Natural Sciences, University of Oslo),
the SIRIUS Centre for Scalable Data Access (Research Council of Norway, project no.: 237898)
and TechnipFMC. We also wish to express our gratitude to TechnipFMC for sharing data with the scientific
community, and to Derek Smith and Marcel Castro at TechnipFMC for providing invaluable support in the
process of publishing the data.

\vskip0.5cm
\textbf{Declaration of Competing Interest}\\

\vskip0.3cm
\begin{itemize}
\item[$\square$\raisebox{.1em}{\hspace{-.6em}$\checkmark$}]
  {The authors declare that they have no known competing financial interests or personal relationships that could have appeared to influence the work reported in this paper.}

\item[$\square$%
  %\raisebox{.1em}{\hspace{-.6em}$\checkmark$}%
]{The authors declare the following financial interests/personal relationships which may be considered as potential competing interests: }
\end{itemize}
\vskip0.3cm
The author (Daniel Bakkelund) holds a position as data scientist at TechnipFMC.

\bibliographystyle{plain}
\bibliography{ms}

\end{document}


