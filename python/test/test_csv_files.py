# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import unittest as ut
import machine_parts_pp as pp

class CsvLoadTest(ut.TestCase):

    def _count_lines(self,fname):
        lines = 0
        with open(fname, 'r') as inf:
            while inf.readline() != '':
                lines += 1

        return lines

    def test_parts(self):
        fname = pp.get_parts_csv_file_name()
        lines = self._count_lines(fname)
        self.assertEqual(150, lines)

    def test_dissims(self):
        fname = pp.get_dissimilarities_csv_file_name()
        lines = self._count_lines(fname)
        self.assertEqual(11175, lines)
