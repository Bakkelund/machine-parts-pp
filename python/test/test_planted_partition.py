# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import unittest as ut
import numpy.testing as npt
import machine_parts_pp as pp

class PlantedPartitionTestCase(ut.TestCase):

    def testCorrectPlantedPartition(self):
        import numpy as np
        C0    = pp.get_machine_part_connected_components(7)
        C0    = pp._complete_dict(C0)
        X0,D0 = pp._gen_rebased_dissim_space(C0)
        M     = 4
        mu    = 0.001
        var   = 0.1
        X,D,PP,(ren,inv) = pp._gen_planted_partition(X0,D0,M,mu,var)

        # Name the pp elements back to their original and verify that they
        # are generated as [0 |C0| 2|C0] ... ], [1 1+|C0| ...] etc
        invPP = [[inv(x) for x in p] for p in PP]
        for elts in zip(invPP):
            elts = np.array(elts[0], dtype=int)
            dElts = elts[1:] - elts[:-1]
            self.assertTrue(np.all(dElts == len(C0)),
                            'deltas are not %d: %s' % (M,str(dElts)))

    def testDissimilarityDist(self):
        import numpy as np
        C0    = pp.get_machine_part_connected_components(7)
        C0    = pp._complete_dict(C0)
        X0,D0 = pp._gen_rebased_dissim_space(C0)
        M     = 4
        mu    = 0.1
        var   = 0.1
        X,D,PP,(ren,inv) = pp._gen_planted_partition(X0,D0,M,mu,var)

        # Check symmetric
        npt.assert_array_equal(D,D.T)
        
        # For each pair of elements
        # 1) rename back to original
        # 2) map to base space
        # 3) compute residual between generated and base
        # 4) check that residuals inside the normal dist
        N0  = len(C0)
        N   = len(X)
        res = []
        for i in range(N):
            for j in range(i+1,N):
                bi = inv(i) % N0
                bj = inv(j) % N0
                res.append(D[i,j] - D0[bi,bj])

        res = np.abs(res)
        std = np.sqrt(var)
        n   = len(res)

        # Check that at least .68 of all residuals are less than one
        # standard dev away from the mean
        r1  = np.sum(res <= std)/n
        self.assertLess(0.68, r1)

        # Check that at least .95 of all residuals are less than two
        # standard dev away from the mean
        r2  = np.sum(res <= 2*std)/n
        self.assertLess(0.95, r2)

        # Check that at least .997 of all residuals are less than three
        # standard dev away from the mean
        r3  = np.sum(res <= 3*std)/n
        self.assertLess(0.997, r3)

    def testDissimMapping(self):
        import numpy as np
        
        c0 = {0:[1,2],1:[2],2:[]}
        d0 = np.array([[.0, .1, .2],
                       [.1, .0, .3],
                       [.2, .3, .0]])
        M   = 2
        mu  = 0
        var = 0
        X,D,PP,_ = pp._gen_planted_partition(c0,d0,M,mu,var,no_rename=True)

        # Check that all pairs when mapped to the base index
        # have dissimilarities equal to the base space dissimilarity
        N = len(X)
        n = len(c0)
        for i in range(N):
            for j in range(i+1,N):
                i0 = i % n
                j0 = j % n
                self.assertEqual(d0[i0,j0],D[i,j],
                                 'd0[%d,%d] != D[%d,%d]' % (i0,j0,i,j))

class TransitiveClosureTestCase(ut.TestCase):

    def test_tc(self):
        raw = {1:[8], 3:[8], 4:[], 6:[], 7:[3,9], 8:[6], 9:[4,8]}
        exp = {1:[6,8], 3:[6,8], 4:[], 6:[], 7:[3,4,6,8,9],
               8:[6], 9:[4,6,8]}

        res = pp._transitive_closure(raw)
        self.assertEqual(exp,res)

class RandomInducedSubgraphTestCase(ut.TestCase):

    def test_exception(self):
        G = pp.get_machine_part_connected_components(0)
        with self.assertRaises(pp.MinDegNotSatisfiableError) as context:
            pp.random_induced_subgraph(G,3,minDeg=10,attempts=10)
